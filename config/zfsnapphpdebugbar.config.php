<?php
define('DEBUGBAR_REGEX_PREFIX', MULTILANGUAGE ? '/[a-z]*' : '');
define('DEBUGBAR_SPEC_PREFIX', MULTILANGUAGE ? '/%lng%' : '');
define('DEBUGBAR_FRONTEND_LNG', empty($_SESSION['frontend_language']) ? _ROOT_LNG_SHORT_ : $_SESSION['frontend_language']);
define('DEBUGBAR_BACKEND_LNG', empty($_SESSION['backend_language']) ? _ROOT_LNG_SHORT_ : $_SESSION['backend_language']);

return [
    'php-debug-bar' => [
        'view' => [
            'custom-style-path' => 'zf-snap-php-debug-bar.css',
            'debugbar-resources' => __DIR__.'/../../../maximebf/debugbar/src/DebugBar/Resources/',
            'custom-resources' => __DIR__.'/../assets/',
        ],
        'enabled' => true,
        'auto-append-assets' => false,
        'render-on-shutdown' => true,
        'zend-db-adapter-service-name' => '', //Zend\Db\Adapter\Adapter::class,
        // ServiceManager service keys to inject collectors
        // http://phpdebugbar.com/docs/data-collectors.html
        'collectors' => [
            // uncomment if you use Doctrine ORM
            //DebugBar\Bridge\DoctrineCollector::class,
        ],
        // ServiceManager service key to inject storage
        // http://phpdebugbar.com/docs/storage.html
        'storage' => null,
    ],
    'service_manager' => [
        'invokables' => [
            DebugBar\DebugBar::class => DebugBar\StandardDebugBar::class,
        ],
        'factories' => [
            'debugbar' => ZfSnapPhpDebugBar\Service\PhpDebugBarFactory::class,
            ZfSnapPhpDebugBar\Log\Writer\PhpDebugBar::class => ZfSnapPhpDebugBar\Log\Writer\PhpDebugBarFactory::class,
            DebugBar\Bridge\DoctrineCollector::class => ZfSnapPhpDebugBar\Collector\DoctrineCollectorFactory::class,
        ],
        'delegators' => [
            // uncomment if you use Doctrine ORM
            //'doctrine.configuration.orm_default' => [
            //    ZfSnapPhpDebugBar\Delegator\DoctrineConfigurationDelegatorFactory::class,
            //],
        ],
    ],
    'controllers' => [
        'factories' => [
            ZfSnapPhpDebugBar\Controller\Resources::class => ZfSnapPhpDebugBar\Controller\ResourcesFactory::class,
        ],
    ],
    'view_helpers' => [
        'factories' => [
            'debugbar' => ZfSnapPhpDebugBar\View\Helper\DebugBarFactory::class,
        ],
    ],
    'router' => [
        'routes' => [
            'phpdebugbar-resource' => [
                'type' => 'regex',
                'options' => [
                    'regex' => DEBUGBAR_REGEX_PREFIX.'/DebugBar/Resources/(?<resource>[a-zA-Z0-9_.\-/]+)',
                    'spec' => DEBUGBAR_SPEC_PREFIX.'/DebugBar/Resources/%resource%',
                    'defaults' => [
                        'langshort'  => DEBUGBAR_FRONTEND_LNG,
                        'backendlanguage'  => DEBUGBAR_BACKEND_LNG,
                        'controller' => ZfSnapPhpDebugBar\Controller\Resources::class,
                        'action' => 'index',
                    ],
                ],
            ],
            'phpdebugbar-custom-resource' => [
                'type' => 'regex',
                'options' => [
                    'regex' => DEBUGBAR_REGEX_PREFIX.'/zfsnapphpdebugbar/resources/(?<resource>[a-zA-Z0-9_.\-/]+)',
                    'spec' => DEBUGBAR_SPEC_PREFIX.'/zfsnapphpdebugbar/resources/%resource%',
                    'defaults' => [
                        'langshort'  => DEBUGBAR_FRONTEND_LNG,
                        'backendlanguage'  => DEBUGBAR_BACKEND_LNG,
                        'controller' => ZfSnapPhpDebugBar\Controller\Resources::class,
                        'action' => 'custom',
                    ],
                ],
            ],
        ],
    ],
];
